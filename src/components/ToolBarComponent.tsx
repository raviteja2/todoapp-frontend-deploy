import React from "react";
import { Toolbar, Typography, AppBar, IconButton } from "@material-ui/core";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import "react-toastify/dist/ReactToastify.css";
import { api } from "../host-link";
toast.configure();
export const ToolBarComponent = () => {
  const history = useHistory();

  const logOut = () => {
    axios.post(api + "/users/logout").then((res) => {
      localStorage.removeItem("token");
    });
    setTimeout(() => {
      history.push("/users/login");
    }, 500);
    toastLogOut();
  };

  const toastLogOut = () => {
    toast.info("User Logged Out", { autoClose: 3000 });
  };
  return (
    <AppBar color="primary">
      <Toolbar>
        <Typography variant="h6">TODO App</Typography>
        <span style={{ marginLeft: "90%" }}>
          <IconButton onClick={logOut}>
            <ExitToAppIcon fontSize="large" />
          </IconButton>
        </span>
      </Toolbar>
    </AppBar>
  );
};
